import * as React from 'react';
import './App.css';
import videojs from 'video.js';
import video from 'videojs-contrib-hls';
import axios from 'axios';
import 'video.js/dist/video-js.css';

import logo from './logo.svg';

class App extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      player: null,
      blobURL: null,
      blob: null,
      beers: null,
      isLoading: null,
    }
  }

  componentDidUpdate() {
    const videoJsOptions = {
      autoplay: true,
      controls: true,
      aspectRatio: "16:9",
    }
    
    let localPlayer = videojs("video-player", {
      html5: {
        hls: {
          //withCredentials: true
        }
      }
    }, function ready() {
      this.addClass('my-example');
    });

    localPlayer.src({
      src: "http://127.0.0.1:8080/video/master.m3u8",
      type: "application/x-mpegURL",
    })

  }

  componentWillMount() {
    this.setState({isLoading: true});

    fetch('http://127.0.0.1:8080/good-beers')
      .then(response => response.json())
      .then(data => this.setState({beers: data, isLoading: false}));
  }

  render() {
  const {beers, isLoading} = this.state;

  if (isLoading) {
    return <p>Loading...</p>;
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to React</h1>
      </header>
      <div>
        <h2>Beer List</h2>
      {beers.map((beer: any) =>
          <div key={beer.id}>
            {beer.name}
          </div>
        )}
      </div>

      <div>
        <video id="video-player" className="video-js vjs-default-skin" autoPlay controls>
          <p className="vj-no-js">
            To view this video, please enable Javascript.
          </p>
        </video>
      </div>

    </div>
  );
}
}

export default App;
