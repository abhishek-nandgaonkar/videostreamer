package com.example.videoStreamer.videoStreamer;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@SpringBootConfiguration
public class VideoStreamerApplicationTests {

	@Test
	public void test() {
		Assert.assertEquals(10, 10);
	}

}
