package com.example.demo.beer;

import lombok.extern.slf4j.XSlf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
public class BeerController {
    private BeerRepository repository;

    public BeerController(BeerRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/good-beers")
    @CrossOrigin(origins = "http://127.0.0.1:3000")
    public Collection<Beer> goodBeers() {
        return repository.findAll().stream()
                .filter(this::isGreat)
                .collect(Collectors.toList());
    }

    @GetMapping("/video/{fileName}/{segment}")
    @CrossOrigin(origins = "http://127.0.0.1:3000")
    public void segments(HttpSession httpSession,
                         HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         String fileName,
                         String segment) {

        try {
            byte[] returnBytes = importFile(segment + ".m3u8");

            if (null == returnBytes) {
                returnBytes = importFile(segment + ".ts");
            }

            int max = returnBytes.length;
            httpServletResponse.setHeader("Accept-Ranges", "bytes");
            httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS, POST");
            httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
            httpServletResponse.setHeader("Access-Control-Allow-Headers", "accept, access-control-allow-origin, authorization, content-type, method");
            httpServletResponse.setHeader("Age", "50000");
            httpServletResponse.setHeader("Connection", "keep-alive");
            httpServletResponse.setHeader("Content-Length", String.valueOf(max));
            httpServletResponse.setHeader("Content-Type", "video/MP2T");
            httpServletResponse.setHeader("ETag", segment + LocalDateTime.now());
            httpServletResponse.setStatus(206);

            ServletOutputStream response = httpServletResponse.getOutputStream();
            response.write(returnBytes);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private byte[] importFile(String filePath) {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(filePath);

        if (null == inputStream) {
            return null;
        }

        try {
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            //log.error("Error occurred while importing the file.");
            e.printStackTrace();
        }
        throw new RuntimeException("Error occurred while importing the file");
    }


    @RequestMapping(value = "/video/{name}", method = RequestMethod.GET)
    @CrossOrigin(origins = "http://127.0.0.1:3000")
    public void master(HttpSession httpSession,
                       HttpServletRequest httpServletRequest,
                       HttpServletResponse httpServletResponse,
                       @PathVariable String name) {
        try {
            byte[] returnBytes = importFile( name );

            int max = returnBytes.length;
            httpServletResponse.setHeader("Accept-Ranges", "bytes");
            httpServletResponse.setHeader("'Access-Control-Allow-Credentials", "true");
            httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS, POST");
            httpServletResponse.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:3000");
            httpServletResponse.setHeader("Access-Control-Allow-Headers", "accept, access-control-allow-origin, authorization, content-type, method");
            httpServletResponse.setHeader("Age", "50000");
            httpServletResponse.setHeader("Connection", "keep-alive");
            httpServletResponse.setHeader("Content-Length", String.valueOf(max));
            httpServletResponse.setHeader("Content-Type", "video/MP2T");
            httpServletResponse.setHeader("ETag", "master" + LocalDateTime.now());
            httpServletResponse.setStatus(206);

            ServletOutputStream response = httpServletResponse.getOutputStream();
            response.write(returnBytes);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private boolean isGreat(Beer beer) {
        return !beer.getName().equals("Budweiser") &&
                !beer.getName().equals("Coors Light") &&
                !beer.getName().equals("PBR");
    }
}